package com.jackfruit.greenscaledevelopers

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

class RvDesignsAdapter:RecyclerView.Adapter<RvDesignsAdapter.ViewHolder>()
{
    override fun onBindViewHolder(p0: ViewHolder, p1: Int) {

    }

    class ViewHolder(layout: View): RecyclerView.ViewHolder(layout)

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): RvDesignsAdapter.ViewHolder {

      return ViewHolder( LayoutInflater.from(p0.context).inflate(R.layout.designlistitem,p0,false))
    }

    override fun getItemCount(): Int {
        return 10
    }


}