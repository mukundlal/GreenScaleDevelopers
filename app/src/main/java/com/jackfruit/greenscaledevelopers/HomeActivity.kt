package com.jackfruit.greenscaledevelopers

import android.os.Bundle
import android.support.design.widget.BottomNavigationView
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.View
import kotlinx.android.synthetic.main.activity_home.*

class HomeActivity : AppCompatActivity() {

    private val mOnNavigationItemSelectedListener = BottomNavigationView.OnNavigationItemSelectedListener { item ->
        when (item.itemId) {
            R.id.navigation_home -> {

                return@OnNavigationItemSelectedListener true
            }
            R.id.navigation_dashboard -> {

                return@OnNavigationItemSelectedListener true
            }
            R.id.navigation_notifications -> {

                return@OnNavigationItemSelectedListener true
            }
        }
        false
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.collapsedlayout)

        rvDesigns.layoutManager=LinearLayoutManager(applicationContext,LinearLayoutManager.VERTICAL,false);
        rvDesigns.adapter=RvDesignsAdapter()
        rvDesigns.addOnScrollListener(object :RecyclerView.OnScrollListener(){

            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                if (dy > 0 && navigation.isShown) {

                    navigation.visibility = View.GONE
                } else if (dy < 0 ) {
                    navigation.visibility = View.VISIBLE

                }
            }

            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {

            }
        }

        )

     //   navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener)
    }
}
